/**
 * @file
 *
 * Applies Elbow room preference.
 *
 * Syntax: ES6
 */

(function setupUI(Drupal, drupalSettings, once) {
  /* global ElbowRoomState */
  const elbowRoomStorage = new ElbowRoomState();

  Drupal.behaviors.ElbowRoom = {
    attach() {
      // Load persistent state if any.
      if (elbowRoomStorage.exists()) {
        drupalSettings.elbow_room.default = elbowRoomStorage.get();
      }

      // Get the form... add a link above
      const link = this.createLink();
      link.addEventListener("click", this.clickLink);
      link.addEventListener("click", this.updateElbowRoomState);

      // Add the link to the form.
      const wrapper = document.createElement("div");
      wrapper.setAttribute("class", "elbow-room-link-wrapper");
      wrapper.appendChild(link);
      once("elbow-room", "form.node-form").forEach((e) =>
        e.parentNode.insertBefore(wrapper, e)
      );
      if (drupalSettings.elbow_room.default) {
        link.click();
      }
    },

    /**
     * Create a link element to add to the node form.
     *
     * @return {HTMLAnchorElement}
     *   A link to toggle the sidebar.
     */
    createLink() {
      const link = document.createElement("a");
      link.innerHTML = "Hide sidebar";
      link.setAttribute("href", "#");
      link.setAttribute("class", "elbow-room-action");
      return link;
    },

    /**
     * Callback function for the click event on the link.
     *
     * @param {object} e
     *    Click event object.
     */
    clickLink(e) {
      if (e.target.classList.contains("has-elbow-room")) {
        e.target.classList.remove("has-elbow-room");
        e.target.innerHTML = Drupal.t("Hide sidebar");
        document
          .querySelectorAll("form.node-form div.layout-node-form")
          .forEach((elem) => elem.classList.remove("elbow-room"));
      } else {
        e.target.classList.add("has-elbow-room");
        e.target.innerHTML = Drupal.t("Show sidebar");
        document
          .querySelectorAll("form.node-form div.layout-node-form")
          .forEach((elem) => elem.classList.add("elbow-room"));
      }
      e.preventDefault();
    },

    /**
     * "Click" event handler...
     *
     * ...for the "Show/Hide sidebar" link.  Saves Elbow room's state after each
     * click.
     *
     * @param {object} e
     *    Click event object.
     */
    updateElbowRoomState(e) {
      const isNotElbowRoomLink =
        !e.target.classList.contains("elbow-room-action");
      if (isNotElbowRoomLink) {
        return;
      }

      const elbowRoomState = e.target.classList.contains("has-elbow-room")
        ? 1
        : 0;

      elbowRoomStorage.set(elbowRoomState);
    },
  };
})(Drupal, drupalSettings, once);
