/**
 * @file
 * Manages Elbow room state persistence.
 */

class ElbowRoomState {
  constructor() {
    this.elbowRoomStorageKey = "Drupal.elbow_room.state";

    this.hasLocalStorage = typeof localStorage !== "undefined";
    this.hasElbowRoomState = this.hasLocalStorage
      ? localStorage.getItem(this.elbowRoomStorageKey) !== null
      : false;
  }

  /**
   * Have we got a persistent state?
   *
   * @return {bool}
   *   Do we have localStorage?
   */
  exists() {
    if (this.hasLocalStorage && this.hasElbowRoomState) {
      return true;
    }

    return false;
  }

  /**
   * Get the persistent state if it exists.
   *
   * @return {(int|null)}
   *   Get Elbow room state.
   */
  get() {
    if (!this.exists()) {
      return;
    }

    const elbowRoomStateString = localStorage.getItem(this.elbowRoomStorageKey);
    const elbowRoomState = parseInt(elbowRoomStateString, 10);

    return elbowRoomState;
  }

  /**
   * Set the persistent state.
   *
   * @param {int} currentState
   *   Current Elbow room state.
   */
  set(currentState) {
    localStorage.setItem(this.elbowRoomStorageKey, currentState);

    this.hasElbowRoomState = true;
  }
}
